# README #

此 Maven 專案用於證明：

* org.owasp.encoder 1.1.1 可解決 Checkmarx 所指出的 Reflacted_XSS_All_Clients 高風險弱點。
* java.text.Normalizer 可解決 Checkmarx 所指出的 Input_Not_Normalized 中風險弱點。

### What is this repository for? ###

* LY

### How do I get set up? ###

* Maven
* Eclipse (optional)
* Java Web Servlet Container (ex: Tomcat)
* Checkmarx

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact