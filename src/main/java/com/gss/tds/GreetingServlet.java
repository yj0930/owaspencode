package com.gss.tds;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.text.Normalizer.Form;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.encoder.Encode;
import org.sqlite.SQLiteDataSource;

/**
 * Servlet implementation class GreetingServlet
 */
public class GreetingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GreetingServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userName = request.getParameter("userName");

		// Medium-risk Input_Not_Normalized fixed by this.
		userName = Normalizer.normalize(userName, Form.NFKC);
		
		if (userName == null) {
			userName = "anonymous";
		}
		else {
			if (userName.equals("")) {
				userName = "blank";
			}
		}
		String result = checkNull(userName);
		databaseOperation();
		PrintWriter printWriter = response.getWriter();
		printWriter.print(result);
		printWriter.close();
	}

	private static String checkNull(String value) {
		
		String returnValue = value;
		
		// High-risk Reflacted_XSS_All_Clients fixed by this.
		returnValue = Encode.forHtml(returnValue);
		
		return returnValue;
	}
	
	private static void databaseOperation() {
		
		SQLiteDataSource ds = new SQLiteDataSource();
		ds.setUrl("jdbc:sqlite:test.db");
		String sql;
		try {
			Connection con = ds.getConnection();
			Statement stat = con.createStatement();
			sql = "DROP TABLE IF EXISTS test;";
			stat.executeUpdate(sql);
			sql = "CREATE TABLE test(column1 string, column2 string);";
			stat.executeUpdate(sql);
			sql = "INSERT INTO test VALUES('value1', 'value2');";
			stat.executeUpdate(sql);
			sql = "SELECT * FROM test;";
			ResultSet rs = stat.executeQuery(sql);
			while (rs.next()) {
				String s1 = rs.getString("column1");
				String s2 = rs.getString("column2");

				// Medium-risk Input_Not_Normalized fixed by this.
				s1 = Normalizer.normalize(s1, Form.NFKC);
				s2 = Normalizer.normalize(s2, Form.NFKC);
				
				if (checkNull(s1).equals(checkNull(s2))) {
					break;
				}
			}
			rs.close();
			stat.close();
			con.close();
		}
		catch (SQLException sqlException) {
			
		}
	}
}
